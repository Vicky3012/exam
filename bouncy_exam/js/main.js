/* ISOTOPE */
function useIsotope(event) {

    // init Isotope
    let isotopeGrid = new Isotope( '#portfolio-images-inner', {
        itemSelector: '.item',
        masonry: {
            // use element for option
            columnWidth: '.item',
            itemSelector: '.item',
            transitionDuration: '0.5s',
            gutter: 8,
            horizontalOrder: true,
        }
    });

    let applyFilterFromLink = (linkObject) => {
        let filterValue = linkObject.dataset.filter;
        isotopeGrid.arrange({ filter: filterValue });
    };

    let filterGrid = function( event ) {
        event.preventDefault();
        applyFilterFromLink(this);
        let activeBtn = document.querySelector('.filter-btn--active');
        if (activeBtn) {
            activeBtn.classList.remove('filter-btn--active');
        }
        this.classList.add('filter-btn--active');
    };

    document.querySelectorAll('.filter-btn').forEach(filterBtn => {
        filterBtn.addEventListener( 'click', filterGrid);
    });

    let activeBtn = document.querySelector('.filter-btn--active');
    applyFilterFromLink(activeBtn);
}
document.addEventListener('DOMContentLoaded', useIsotope);

/* END ISOTOPE */

/* SLICK SLIDER */
$(document).ready(function(){
    $('.team__carousel').slick({
        arrows: false,
        dots: true,
    });
});

$(document).ready(function(){
    $('.comments').slick({
        arrows: false,
        dots: true,
        autoplay: true,
        autoplaySpeed: 4000,
    });
});
/* END SLICK SLIDER */

/* GOOGLE MAP */
var map;
function initMap() {
    map = new google.maps.Map(document.querySelector('.contact-us__map'), {
        center: {lat: 47.8168972, lng: 35.1758627},
        zoom: 14,
        scrollwheel: false
    });

    var image = 'images/beetroot-marker.png';
    var marker = new google.maps.Marker({
        position: {lat: 47.8215763, lng: 35.1655094},
        map: map,
        icon: image
    });

    var infowindow = new google.maps.InfoWindow({
        content: 'We Are Here!'
    });
    marker.addListener('click', function() {
        infowindow.open(map, marker);
    });
}
document.addEventListener('DOMContentLoaded', initMap);
/* GOOGLE MAP END*/

/* SMOOTH SCROLL */ 

smoothScroll.init({
      selector: '[data-scroll]',
      selectorHeader: null,
      speed: 500,
      easing: 'ease',
      offset: 0
  });
/* SMOOTH SCROLL END*/